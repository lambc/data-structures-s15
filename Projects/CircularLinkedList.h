#ifndef CIRCULAR_LINKED_LIST_H_
#define CIRCULAR_LINKED_LIST_H_

#include <iostream>
#include <cstdlib>

template <typename T> class List;
template <typename T> std::ostream& operator<<(std::ostream& out, const List<T>& list);

template <class T>
class List {

    //The privately declared Node class for list items
    private:
        class Node {
            public:
                T data;
                Node* next;
        };

    public:
        List();
        List(const List& originalList);

        ~List();

        int getSize() const;
        bool isEmpty() const;
        bool insert(const T& item, const int& position);
        bool remove(const int& positon);
        void display(std::ostream& out) const;

        T runJospehusAlgorithm();

        const List& operator=(const List& rhs);
        friend std::ostream& operator<< <>(std::ostream& out, const List<T>& list);

    private:
        Node* _first; // pointer to the first element of the linked list
        int _size;

        bool copyList(Node* originalListFirst, Node*& copiedListFirst);
        bool deleteList(Node* firstPtr);
};

template <class T>
List<T>::List()
{
    _first = 0;
    _size = 0;
}

template <class T>
List<T>::List(const List& originalList)
{
    // initialize an empty list
    _first = 0;
    _size = originalList._size;

    // if the original list is empty, don’t do anything else
    if (originalList._size==0) {
        return;
    }

    // otherwise copy the list
    copyList(originalList._first, _first);
}


template <class T>
List<T>::~List()
{
    deleteList(_first);
}

template <class T>
bool List<T>::isEmpty() const
{
    return _size==0;
}

template <class T>
void List<T>::display(ostream& out) const
{
    Node * ptr = _first;
    while (ptr != 0)
    {
        out << ptr->data << " ";
        ptr=ptr->next;
    }
}


template <class T>
bool List<T>::insert(const T& item, const int& position)
{
    // verify that this is an acceptable position
    if (position<0 || position>_size) {
        return false;
    }

    // create a new node with the specified data
    Node * newNode = new Node;
    newNode->data = item;
    newNode->next = 0;

    // if this is the new head, then update _first
    if (position==0)
    {
        if (_first==0)
        {
            _first = newNode;
        }
        else
        {
            newNode->next=_first;
            _first = newNode;
        }
    }
    else
    {
        // go to the appropriate position in the linked list to insert the item
        Node * predPtr = _first;
        for (int i=1; i<position; ++i) {
            predPtr = predPtr->next;
        }

        // insert the new element
        newNode->next = predPtr->next;
        predPtr->next = newNode;
    }

    // increment the size of the current list
    ++_size;
    return true;
}

template <class T>
bool List<T>::remove(const int& position)
{
    // verify we can delete an element
    if (_size==0) {
        return false;
    }

    // verify this is a valid item to delete
    if (position<0 || position>=_size) {
        return false;
    }

    // the first item is a special case
    if (position==0) {
        Node * ptr = _first;
        _first = _first->next;
        delete ptr;
    }
    else
    {
        // go to the appropriate position in the linked list to delete the item
        Node * ptr = _first;
        Node * predPtr;
        for (int i=0; i<position; ++i) {
            predPtr = ptr;
            ptr = ptr->next;
        }

        // bypass the item to be deleted
        predPtr->next = ptr->next;
        // free the memory for the item to be deleted
        delete ptr;
    }

    --_size;
    return true;
}

template <class T>
const List<T>& List<T>::operator=(const List& rhs)
{
    // verify this is not a self-assignment
    if (this == &rhs) {
        return *this;
    }
    _size = rhs._size;

    // copy rhs’s elements into a new list
    copyList(rhs->_first,_first);
    return *this;
}


template <class T>
bool List<T>::copyList(Node * originalListFirst, Node * &copiedListFirst)
{
    // if the original list is empty, then simply delete the anything held in
    // the copied list
    if (originalListFirst == 0) {
        deleteList(copiedListFirst);
        return true;
    }

    // if there is already a list held where the copied list should go, then
    // delete that list
    deleteList(copiedListFirst);

    // copy the data from the first node
    copiedListFirst = new Node;
    copiedListFirst->data = originalListFirst->data;
    copiedListFirst->next = 0;

    // create a pointer to the current location in each list
    Node * originalPtr = originalListFirst;
    Node * copiedPtr = copiedListFirst;

    // copy the data in the remaining nodes
    while (originalPtr->next!=0)
    {
        copiedPtr->next = new Node;
        originalPtr = originalPtr->next;
        copiedPtr = copiedPtr->next;
        copiedPtr->data = originalPtr->data;
        copiedPtr->next = 0;
    }
    copiedPtr->next = 0;
    return true;
}


template <class T>
bool List<T>::deleteList(Node * firstPtr)
{
    if (firstPtr == 0) {
        return true;
    }

    Node * ptr = firstPtr;
    while (ptr != 0) {
        Node * tempPtr = ptr;
        ptr = ptr->next;
        delete tempPtr;
    }

    firstPtr = 0;
    return true;
}


#endif // CIRCULAR_LINKED_LIST_H_
