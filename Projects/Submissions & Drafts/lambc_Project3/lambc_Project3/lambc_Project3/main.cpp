#include "Tolls.h"

#include <iostream>

using namespace std;

int main()
{	
	Tolls myToll;

	myToll.openFile();
	myToll.display();
	
	cout << "Program execution has ended. Please press enter to exit.";
	cin.get();
	return 0;
}