/* Project 3 ------------------------------------------------------------------
Christopher Lamb UID 41662305
Date Due: 04/14/2015
-----------------------------------------------------------------------------*/
#ifndef TOLLS_H_
#define TOLLS_H_

#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <map>
#include <array>

using namespace std;

ifstream tollInputFile("input.txt");
ofstream tollOutputFile("lambc.output.txt");

struct mapValues {
	int enteredAt = 0;
	int rateAtEnteredHour = 0;
	int distanceTravelledAtTrip = 0;
	float billTotal;
	int adminFees = 2;
	int timesEntered = 1;
} tollMapData;

class Tolls
{
public:
	Tolls();
	void openFile();
	void lineType();
	void display();
	void mapping();
	void tagToMap();
	void rateLineToArray();
	void clearForNewCase();

private:
	map<string, mapValues> tollMap;
	map<string, mapValues>::iterator it;
	string inputLine;
	bool isTag = false;
	bool isCase = false;
	bool isRate = false;
	bool isSpace = false;
	int newCase = 0;
	array<int, 25> rates;
};

//-- Constructors
Tolls::Tolls()
/*-----------------------------------------------------------------------------
Default constructor
Precondition: None
Postcondition: None

Time Complexity: O(1)
-----------------------------------------------------------------------------*/
{}

//-- Functions
void Tolls::openFile()
/*-----------------------------------------------------------------------------
* Description: Verify that the input file can be opened. If it can be opened 
*	then go to mapping function.
*
* Precondition: input.txt provided
*
* Postcondition: None
*
* Time Complexity: O(n)
-----------------------------------------------------------------------------*/
{
	//O(1)
	if (!tollInputFile)
	{
		cerr << "Oh no! Could not open input.txt for reading toll data." << endl;
		exit(1);
	}
	//O()
	else
	{
		while (tollInputFile)
		{
			mapping();
		}
	}
}

void Tolls::mapping()
/*-----------------------------------------------------------------------------
* Description: Reads in a line from the input file. Then moves it to its keyed
*	location in the map.
*
* Precondition: input.txt has been opened successfully.
*
* Postcondition: The map has recieved values, and all computations on the data
*	have been completed successfully.
*
* Time Complexity: O(n)
-----------------------------------------------------------------------------*/
{
	//O(1)
	getline(tollInputFile, inputLine);

	//O(1)
	lineType();

	//O(1)
	if (isTag == true)
		tagToMap();
	//O(n)
	else if (isSpace == true)
		clearForNewCase();
	//O(n)
	else if (isRate == true)
		rateLineToArray();	
}

void Tolls::lineType()
/*-----------------------------------------------------------------------------
* Description: Uses blank space locations to determine type of data on line.
*
* Precondition: line was read from input.txt
*
* Postcondition: bool types for line type
*
* Time Complexity: O(1)
-----------------------------------------------------------------------------*/
{
	//O(1)
	if (inputLine.size() > 7 && inputLine.at(6) == ' ')
		isTag = true;
		
	//Check for hourly rates line O(1)
	else if (inputLine.size() > 7 && inputLine.at(2) == ' ')
		isRate = true;
	
	//Check for blank line O(1)
	else if (inputLine.length() == 0)
	{
		isSpace = true;
		newCase++;
	}
}

void Tolls::rateLineToArray()
/*-----------------------------------------------------------------------------
* Description: Move toll rates by hour into an array for quick use in calculations.
*	Will be overwritten after each case. In the next case.
*
* Precondition: Input.txt has been read from, and the current line is the hourly
*	rate line. 
*
* Postcondition: An array consisting of hourly rates has been created and populated.
*
* Time Complexity: O(n)
-----------------------------------------------------------------------------*/
{
	stringstream os(inputLine);
	string temp;
	//O(n)
	for (int i = 0; os >> temp; i++)
		rates[i] = stoi(temp);
	isRate = false;
}

void Tolls::clearForNewCase()
/*-----------------------------------------------------------------------------
* Description: Removes data from map for next case.
*
* Precondition: map is not empty and contains values
*
* Postcondition: isSpace is set to false for next iterations, and map has been
*	cleared.
*
* Time Complexity: O(n)
-----------------------------------------------------------------------------*/
{
	if (isSpace == true)
	{
		//Prevents extra display from occuring before first case. O(n)
		if (newCase > 1)
		{
			//O(n)
			display();
			tollOutputFile << endl;
			//O(n)
			tollMap.clear();
		}
	}
	isSpace = false;
}

void Tolls::tagToMap()
/*-----------------------------------------------------------------------------
* Description: Determines if the current line is an enter or exit ramp case. If
*	an enter case it checks if the tag exists in the map. If not it emplaces the
*	default data and the current entrance into the struct. If second enter it 
*	increments the number of times entered for the $1 fee. If it is an exit it
*	performs the basic arithmatic using current exit ramp value and entrance ramp
*	to calculate distance travelled. Pushing all data to the map after each case
*	is completed.
*
* Precondition: input.txt could be read from, and the current line is a tag data
*	line.
*
* Postcondition: Tag data has been moved to the map and correctly calculated.
*
* Time Complexity: O(1)
-----------------------------------------------------------------------------*/
{
	//O(1)
	if ((tollMap.find(inputLine.substr(0, 6)) == tollMap.end()) && (inputLine.at(23) == 'r'))
	{
		tollMapData.enteredAt = stoi(inputLine.substr(25));
		tollMapData.rateAtEnteredHour = rates[stoi(inputLine.substr(13, 1))];
		tollMap.emplace(inputLine.substr(0, 6), tollMapData);
	}
	//O(1)	
	else if (inputLine.at(22) == 't')
	{
		
		tollMapData.distanceTravelledAtTrip += abs(stoi(inputLine.substr(24)) - tollMapData.enteredAt);
		tollMapData.billTotal = tollMapData.distanceTravelledAtTrip * (tollMapData.rateAtEnteredHour * .01);

		//O(1)
		it = tollMap.find(inputLine.substr(0, 6));
		it->second = tollMapData;
	}
	//O(1)
	else if (inputLine.at(23) == 'r')
	{
		//Change rate each time the same car enters.
		if (tollMapData.timesEntered > 1)
			tollMapData.rateAtEnteredHour = rates[stoi(inputLine.substr(13, 1))];

		tollMapData.distanceTravelledAtTrip -= tollMapData.enteredAt;
		tollMapData.timesEntered++;

		//O(1)
		it = tollMap.find(inputLine.substr(0, 6));
		it->second = tollMapData;
	}

	isTag = false;
}

void Tolls::display()
/*-----------------------------------------------------------------------------
* Description: Outputs the contents of the map, and performs calculations for
*	bill totals on admin fees and fees per entrance. Outputs to a file.
*
* Precondition: Map is not empty
*
* Postcondition: Map was output
*
* Time Complexity: Only one for loop. O(n)
-----------------------------------------------------------------------------*/
{
	if (!tollOutputFile)
	{
		cerr << "lambc_output.txt could not be opened for writing." << endl;
		exit(1);
	}
	
	//O(n)
	for (auto& x : tollMap)
		tollOutputFile << x.first << " : $" << x.second.billTotal + x.second.timesEntered + x.second.adminFees << endl;
}
#endif 