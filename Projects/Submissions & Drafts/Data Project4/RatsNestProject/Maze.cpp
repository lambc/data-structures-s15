#include "Maze.h"
#include "Position.h"
#include <queue>
#include <iostream>
#include <cassert>
#include <iomanip>
#include <cstdlib>

using namespace std;

Maze::Maze(Position s, Position e, int n)
{
    size = n;
    start = s;
    exitpos = e;
    try {
        M = new State *[size];
    }
    catch(bad_alloc) {
        cerr << "Unable to allocate array of state pointers";
        exit(1);
    }
    for(int i = 0; i < size; i++) {
        try {
            M[i] = new State[size];
        }
        catch (bad_alloc) {
            cerr << "Unable to allocate row of state values";
            exit(1);
        }
        for (int j = 0; j < size; j++)
            M[i][j] = WALL;
    }
    try {
        Pred = new Position *[size];
    }
    catch(bad_alloc) {
        cerr << "Unable to allocate array of predecessor positions";
        exit(1);
    }
    for(int i = 0; i < size; i++) {
        try {
            Pred[i] = new Position[size];
        }
        catch (bad_alloc) {
            cerr << "Unable to allocate row of predecessor positions";
            exit(1);
        }
        for (int j = 0; j < size; j++)
            Pred[i][j] = Position(i, j);
    }
}

Maze::~Maze()
{
    for(int i = 0; i < size; ++i) {
        delete [] M[i];
        delete [] Pred[i];
    }
    delete [] M;
    delete [] Pred;
}

State
Maze::getState(const Position &P) const
{
    return M[P.getRow()][P.getCol()];
}

void Maze::display(ostream &out) const
{
    out << '\n';
    for (int i=0; i < size;i++) {
        for (int j=0; j < size; j++)
            if (Position(i,j) == start)
                cout << 's';
            else if (Position(i,j) == exitpos)
                cout << 'e';
            else if (M[i][j] == WALL)
                out << '*';
            else
                out << ' ';
        out << '\n';
    }
    out << '\n';
}

void Maze::setState(const Position &P, State s)
{
    int i = P.getRow();
    int j = P.getCol();
    assert(1 <= i && i <= size && 1 <= j && j <= size);
    M[i][j] = s;
}

bool Maze::findExitPath()
{
    // Fill in the missing code for DFS
    // Returns true if a start-to-exit path has been found
    // and the contents of the path stack from to bottom
    // to top will be the exit path
}


bool Maze::findShortestPath()
{
    // Fill in the missing code for BFS
    // Returns true if a start-to-exit path has been found
    // and this path will be a shortest possible escape path
    // This function will fill in the values of the Pred array,
    // which is used to find the shortest path

    queue<Position> q;
}

void Maze::printExitPath(ostream& out)
{
    if (path.empty())
        return;
    Position hold = path.top();
    path.pop();
    printExitPath(out);
    cout << hold << endl;
}

void Maze::printShortestPath(ostream& out)
{
    Position P = exitpos;
    while(!(P == start)) {
        path.push(P);
        P = Pred[P.getRow()][P.getCol()];
    }
    path.push(start);

    while(!path.empty()) {
        out << path.top() << endl;
        path.pop();
    }
}

