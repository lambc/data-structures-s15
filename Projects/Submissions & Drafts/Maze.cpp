/*-----------------------------------------------------------------------------
Code Contributors: Mai Pham (U97637993), Christopher Lamb (U41662305)
-----------------------------------------------------------------------------*/
#include "Maze.h"
#include "Position.h"
#include <queue>
#include <iostream>
#include <cassert>
#include <iomanip>
#include <cstdlib>

using namespace std;

Maze::Maze(Position s, Position e, int n)
{
	size = n;
	start = s;
	exitpos = e;
	try {
		M = new State *[size];
	}
	catch (bad_alloc) {
		cerr << "Unable to allocate array of state pointers";
		exit(1);
	}
	for (int i = 0; i < size; i++) {
		try {
			M[i] = new State[size];
		}
		catch (bad_alloc) {
			cerr << "Unable to allocate row of state values";
			exit(1);
		}
		for (int j = 0; j < size; j++)
			M[i][j] = WALL;
	}
	try {
		Pred = new Position *[size];
	}
	catch (bad_alloc) {
		cerr << "Unable to allocate array of predecessor positions";
		exit(1);
	}
	for (int i = 0; i < size; i++) {
		try {
			Pred[i] = new Position[size];
		}
		catch (bad_alloc) {
			cerr << "Unable to allocate row of predecessor positions";
			exit(1);
		}
		for (int j = 0; j < size; j++)
			Pred[i][j] = Position(i, j);
	}
}

Maze::~Maze()
{
	for (int i = 0; i < size; ++i) {
		delete[] M[i];
		delete[] Pred[i];
	}
	delete[] M;
	delete[] Pred;
}

State
Maze::getState(const Position &P) const
{
	return M[P.getRow()][P.getCol()];
}

void Maze::display(ostream &out) const
{
	out << '\n';
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++)
			if (Position(i, j) == start)
				cout << 's';
			else if (Position(i, j) == exitpos)
				cout << 'e';
			else if (M[i][j] == WALL)
				out << '*';
			else
				out << ' ';
			out << '\n';
	}
	out << '\n';
}

void Maze::setState(const Position &P, State s)
{
	int i = P.getRow();
	int j = P.getCol();
	assert(1 <= i && i <= size && 1 <= j && j <= size);
	M[i][j] = s;
}

bool Maze::findExitPath()
/*-----------------------------------------------------------------------------
Description: Function follows the algorithm for depth first search to find the
	exit path.

Precondition: Input files contains valid data.
Postcondition: Shortest path positions has been moved to path stack.

Time Complexity: O(n)
-----------------------------------------------------------------------------*/
{
	//Start at a given vertex V, and push it onto the stack
	path.push(start);
	setState(start, VISITED);
	Position current = path.top();

	//While the stack is not empty
	while(!path.empty())
	{
		/*For all if and else if statements below:
			visit the first neighbor w of v
			Then visit other neighbor of V
			When a dead end is reached back up to the last node
				examine the remaining*/
		if (getState(current.Neighbor(DOWN)) == OPEN)
		{
			setState(current.Neighbor(DOWN), VISITED);
			path.push(current.Neighbor(DOWN));
		}
		else if (getState(current.Neighbor(LEFT)) == OPEN)
		{
			setState(current.Neighbor(LEFT), VISITED);
			path.push(current.Neighbor(LEFT));
		}
		else if (getState(current.Neighbor(UP)) == OPEN)
		{
			setState(current.Neighbor(UP), VISITED);
			path.push(current.Neighbor(UP));
		}		
		else if (getState(current.Neighbor(RIGHT)) == OPEN)
		{
			setState(current.Neighbor(RIGHT), VISITED);
			path.push(current.Neighbor(RIGHT));
		}
		else
		{
			//If not open paths then remove from stack
			path.pop();
		}
		if(path.top() == exitpos)
		{
			return true;
		}
		current = path.top();
	}
	return false;
}


bool Maze::findShortestPath()
/*-----------------------------------------------------------------------------
Description: Function follows the algorithm for breadth first search to find the
	shortest path.

Precondition: Input files contains valid data.
Postcondition: Shortest path positions has been moved to pred array.

Time Complexity: O(n)
-----------------------------------------------------------------------------*/
{
	//Visit the start vertex
	setState(start, VISITED);

	//Initialize queue to contain only the start vertex
	queue<Position> q;
	q.emplace(start);
	Position current = q.front();

	// While queue not empty O(n)
	while (!(q.empty()))
	{
		/*For all if statements below
			all verticies w adjacent to v do
			if w has not been visited then:
				visit w
				add w to queue*/	
		if (getState(current.Neighbor(DOWN)) == OPEN)
		{
			Pred[current.Neighbor(DOWN).getRow()][current.Neighbor(DOWN).getCol()] = current;
			setState(current.Neighbor(DOWN), VISITED);
			q.emplace(current.Neighbor(DOWN));
		}
		if (getState(current.Neighbor(LEFT)) == OPEN)
		{
			Pred[current.Neighbor(LEFT).getRow()][current.Neighbor(LEFT).getCol()] = current;
			setState(current.Neighbor(LEFT), VISITED);
			q.emplace(current.Neighbor(LEFT));
		}
		if (getState(current.Neighbor(UP)) == OPEN)
		{
			Pred[current.Neighbor(UP).getRow()][current.Neighbor(UP).getCol()] = current;
			setState(current.Neighbor(UP), VISITED);
			q.emplace(current.Neighbor(UP));
		}
		if (getState(current.Neighbor(RIGHT)) == OPEN)
		{
			Pred[current.Neighbor(RIGHT).getRow()][current.Neighbor(RIGHT).getCol()] = current;
			setState(current.Neighbor(RIGHT), VISITED);
			q.emplace(current.Neighbor(RIGHT));
		}

		if (q.front() == exitpos)
		{
			return true;
		}

		//Remove a vertex v from the queue
		q.pop();
		current = q.front();
	}
	return false;

}

void Maze::printExitPath(ostream& out)
{
	//make a copy so we can print this path to both cout and file
	stack<Position> pathCopy(path);

	//Reverse the copied list
	stack<Position> reversedCopy;
	while (!pathCopy.empty())
	{
		reversedCopy.push(pathCopy.top());
		pathCopy.pop();
	}

	//output to stream
	while (!reversedCopy.empty())
	{
		out << reversedCopy.top() << endl;
		reversedCopy.pop();
	}
}

void Maze::printShortestPath(ostream& out)
{
	Position P = exitpos;
	while (!(P == start)) {
		path.push(P);
		P = Pred[P.getRow()][P.getCol()];
	}
	path.push(start);

	while (!path.empty()) {
		out << path.top() << endl;
		path.pop();
	}
}

