#include <iostream>
#include "functions.h"

using namespace std;

int main()
{
    
    /***********************************
    Prime Factors Tests
    ***********************************/
    // Provided test case
    functions testPrimeFactors;
    testPrimeFactors.primeFactors(3960);
    
    // Valid case test
    functions testPrimeFactors00;
    testPrimeFactors00.primeFactors(36);

    // Test for invalid case
    functions testPrimeFactors01;
    testPrimeFactors01.primeFactors(1);

    /***********************************
    Airplane Simulator Tests
    ***********************************/
    functions testAirSim00;
    testAirSim00.airportSim(100, 0, 0, 12, 5);

    functions testAirSim01;
    testAirSim00.airportSim(25, 5, 2, 12, 5);

    functions testAirSim02;
    testAirSim00.airportSim(25, 6, 2, 12, 4);

    cout << "PRESS ENTER TO TERMINATE THE PROGRAM" << endl;
    cin.get();
    return 0;
}