/* Project 2 ------------------------------------------------------------------
Christopher Lamb UID 41662305
Date Due: 2/26/2015
-----------------------------------------------------------------------------*/
#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <stdbool.h>
#include <iostream>
#include <stack>
#include <queue>
#include <array>
#include <math.h>
#include <time.h>

using namespace std;

class functions 
{
public:
    functions();
    stack<int> primeFactors(int);
    bool isReady(int);
    array<int, 4> airportSim(int, int, int, int, int);

private:
    // Prime factors
    stack<int> primeStack;                  // Stack to be returned for prime
                                            // factors.
    
    // Airplane Simulator
    array<int, 4> simulationReturnArray;    // Array used to return information
                                            // used in the airport simulation.
    queue<int> landing;                     // Queue for planes ready to land
    queue<int> takeoff;                     // Planes ready to takeoff
};

//-- Constructors
functions::functions() { }
/*-----------------------------------------------------------------------------
Default constructor
Precondition: None
Postcondition: None

Time Complexity: O(1)
-----------------------------------------------------------------------------*/

//-- Functions
bool functions::isReady(int rate)
/*-----------------------------------------------------------------------------
* Determine if a new plane is ready for either takeoff
* or landing according to the rate passed in. If this function
* is called 60 times, the number of returned trues is approximately
* equal to the original rate passed in.

* Precondition: A rate defining the average number of
* departures/arrivals per hour.
* Postcondition: A bool indicating if there is a plane
* ready to land/takeoff (dependent upon the rate passed in)

* Time Complexity: Theta(1)
-----------------------------------------------------------------------------*/
{
    // rate/60 gives us the likelihood that the indicated
    //action will happen.
    rate = (int)(((float)rate / 60.0) * 100);

    if (rand() % 100 <= rate) return true;
    else                   return false;
}

stack<int> functions::primeFactors(int n)
/*-----------------------------------------------------------------------------
Calculate the prime factors for a given number n. 

Precondition: Integer n is given. This number is the number to find the prime
factors of.

Postcondition: A stack containing the prime factors of n.

Time complexity: O(n^2)
-----------------------------------------------------------------------------*/
{
    // Was n a valid number??
    if (n <= 1 || n == NULL) {
        primeStack.push(NULL);
        cout << "***********************" << endl;
        cout << primeStack.top() << " is not a valid input." << endl;
        cout << "***********************" << endl;
        cout << endl;
        return primeStack;
    }

    int primeQuotient;  // Holds quotient during calculations
    int pushDivisor;    // Holds completed divisor for pushing onto stack
    int tempQuotient;   // Holds calculation of quotient until evenly divided
    int currentN = n;   // Holds the value of n at current calculated value

    // Loop until a non divisible number is found.
    while (currentN > 1) {
        // Find the smallest number that evenly divides the number currently held
        for (int primeDivisor = currentN; primeDivisor > 1; primeDivisor--) {
            tempQuotient = currentN / primeDivisor;

            /* Check for even division by multiplying the divisor by the quotient
            produced by dividing currentN by the primeDivisor. Then compare this
            number with currentN to make sure there was no truncation. Store into
            variables that hold the smallest values as they are compared.*/
            if ((tempQuotient * primeDivisor) == currentN) {
                pushDivisor = primeDivisor;
                primeQuotient = tempQuotient;
            }
        }

        /* Push final prime number into the stack. currentN reached a non divisible
        number. */
        currentN = primeQuotient;
        primeStack.push(pushDivisor);
        pushDivisor = 0;    // Reset to 0 before next loop
    }
    
    // Display prime factors
    cout << "***********************" << endl;
    cout << "Prime Factors: ";
    while (primeStack.empty() == false) {
        cout << primeStack.top() << " ";
        primeStack.pop();
    }
    cout << endl;
    cout << "***********************" << endl;
    cout << endl;

    return primeStack;
}

array<int, 4> functions::airportSim(int totalSimTime, int landingTime, int takeoffTime, int landingRate, int takeoffRate)
/*-----------------------------------------------------------------------------
Precondition:   * totalSimTime = total time that the simulator is to run using the
                    simulated clock.
                * landingTime = time passed in for starting time of last arrival
                    to give pseudo random value for seeding.
                * takeoffTime = time passed in for starting time of last arrival
                    to give pseudo random value for seeding.
                * landingRate = time to pass to before popping queue.
                * takeoffRate = time to pass to before popping queue.

Postcondition: Array containing the following in its elements:
                * simulationReturnArray[0] = Average landing queue length
                    (totalPossible / actual amount inserted)
                * simulationReturnArray[1] = Average time spent in landing queue
                    (sum of wait times / length of queue)
                * simulationReturnArray[2] = Average takeoff queue length
                    (totalPossible / actual amount inserted)
                * simulationReturnArray[3] = Average time spent in takeoff queue
                    (sum of wait times / length of queue)
                

Time complexity: O(n)
-----------------------------------------------------------------------------*/
{
    int arrivingQueueLength = 1;    // Used to divide sum to average arrive time
    int departingQueueLength = 1;   // Used to divide sum to average depart time
    int arrivingSum = 0;            // Used as the sum for arrive time
    int departingSum = 0;           // Used as the sum for depart time

    int lastArrival = landingTime;    // Holds time from last arrival for time calculation
    int lastDepart = takeoffTime;     // Holds time from last departure for time calculation

    bool departingRunwayAvailable = false;  // Flags the runway for use to depart
    
    // Loop used to simulate a clock with each iteration.
    for (int clock = 0; clock <= totalSimTime; clock++) {
        // Check if a plane is ready to land. Then add to queue
        if (isReady(landingRate)) {
            arrivingSum += clock - lastArrival;
            lastArrival = clock;
            ++arrivingQueueLength;
            landing.push(clock * 5);    // Uses clock * 5 to uniquely ID the plane
            cout << "Plane " << landing.back() << " was just added to the arriving queue." << endl;
        }

        // Add departing planes to queue.
        if (isReady(takeoffRate)) {
            departingSum += clock - lastDepart;
            lastDepart = clock;
            ++departingQueueLength;
            takeoff.push(clock * 3);    // Uses clock * 3 to uniquely ID the plane
            cout << "Plane " << takeoff.back() << " was just added to the departing queue." << endl;
        }

        // Frees the runway for departing planes if there are no arriving planes.
        if (landing.empty())
            departingRunwayAvailable = true;

        /* Take the modulus of the clock to the landingRate to pop a plane from
        the arrival queue only if landingRate in time has passed since the last
        arrival. Pop the plane at the front if the queue is not empty.*/
        if (clock % landingRate == 0) {
            if (landing.empty())
                cout << "There are no planes in the arrival queue.\n" << endl;

            else {
                cout << "Plane " << landing.front() << " is preparing to land.\n" << endl;
                landing.pop();
            }        
        }

        /* Take the modulus of the clock to the takeoffRate to pop a plane from
        the takeoff queue only if takeoffRate in time has passed since the last
        takeoff AND the runway is available for departures. Pop a plane if the 
        queue is not empty.*/
        if ((clock % takeoffRate == 0) && departingRunwayAvailable == true) {
            if (takeoff.empty())
                cout << "There are no planes in the takeoff queue.\n" << endl;

            else {
                cout << "Plane " << takeoff.front() << " is preparing to takeoff.\n" << endl;
                takeoff.pop();
            }
        }

        departingRunwayAvailable = false;   // Reset to false for next iteration
    }

    // Set values into the array for return.
    // If no planes arrived or landed then set all to zero in array
    if ((arrivingQueueLength == 0) && (departingQueueLength == 0)) {
        simulationReturnArray[0] = 0;
        simulationReturnArray[1] = 0;
        simulationReturnArray[2] = 0;
        simulationReturnArray[3] = 0;
    }
    /* If no planes arrived in timeframe then set arrivals to zero
    and departures to their corresponding values.*/
    else if (arrivingQueueLength == 0) {
        simulationReturnArray[0] = 0;
        simulationReturnArray[1] = 0;
        simulationReturnArray[2] = (int)ceil(totalSimTime / departingQueueLength);
        simulationReturnArray[3] = (int)ceil(departingSum / departingQueueLength);
    }
    /* If no planes departed in timeframe then set departures to zero
    and arrival to their corresponding values.*/
    else if (departingQueueLength == 0) {
        simulationReturnArray[0] = (int)ceil(totalSimTime / departingQueueLength);
        simulationReturnArray[1] = (int)ceil(arrivingSum / arrivingQueueLength);
        simulationReturnArray[2] = 0;
        simulationReturnArray[3] = 0;
    }
    else {
        simulationReturnArray[0] = (int)ceil(totalSimTime / departingQueueLength);
        simulationReturnArray[1] = (int)ceil(arrivingSum / arrivingQueueLength);
        simulationReturnArray[2] = (int)ceil(totalSimTime / departingQueueLength);
        simulationReturnArray[3] = (int)ceil(departingSum / departingQueueLength);
    }

    // Display array contents
    cout << "Average landing queue length: " << simulationReturnArray[0] << endl;
    cout << "Average time spent in landing queue: " << simulationReturnArray[1] << endl;
    cout << "Average takeoff queue length: " << simulationReturnArray[2] << endl;
    cout << "Average time spent in takeoff queue: " << simulationReturnArray[3] << endl;
    cout << "\n" << endl;
    cout << "*************************************************" << endl;
    cout << "*********** END OF AIRPLANE SIMULATOR ***********" << endl;
    cout << "*************************************************\n" << endl;

    return simulationReturnArray;
}

#endif // FUNCTIONS_H_
